# Generated by Django 3.0.2 on 2020-10-14 22:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('campi', '0002_auto_20200409_1151'),
    ]

    operations = [
        migrations.CreateModel(
            name='Line',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_lat', models.FloatField()),
                ('start_lng', models.FloatField()),
                ('end_lat', models.FloatField()),
                ('end_lng', models.FloatField()),
                ('campus', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='campi.Campus')),
            ],
        ),
    ]
